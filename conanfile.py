import os

from conans import ConanFile, CMake

project_name = "gitlab_project_tools"


class GitlabProjectToolsConan(ConanFile):
    name = "asd.%s" % project_name
    version = "0.0.1"
    license = "MIT"
    author = "bright-composite"
    url = "https://gitlab.com/asd-project/%s" % project_name
    description = "Tools for managing projects hosted on GitLab"
    topics = ("asd", project_name)
    generators = "cmake"
    settings = "os", "compiler", "build_type", "arch"
    exports_sources = "src*", "CMakeLists.txt", "asd.json"
    requires = (
        "asd.launch/0.0.1@asd/testing",
        "asd.flow/0.0.1@asd/testing",
        "asd.fs/0.0.1@asd/testing",
        "asd.http_client/0.0.1@asd/testing",
        "zlib/1.2.12",
        "cli11/[>=1.9.0]",
        "rang/[>=3.1.0]"
    )

    def source(self):
        pass

    def build(self):
        cmake = CMake(self)
        cmake.definitions['CMAKE_MODULE_PATH'] = self.deps_user_info["asd.build_tools"].module_path
        cmake.configure()
        cmake.build()

    def package(self):
        self.copy("*.h", src="include", dst="include")
        self.copy("*gitlab_project_tools*", src="bin", dst="bin", keep_path=False)
