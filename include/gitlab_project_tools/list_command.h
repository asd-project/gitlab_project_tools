//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gitlab_project_tools/common.h>

//---------------------------------------------------------------------------

namespace asd
{
    inline void add_list_command(CLI::App & app, http::connection & connection, const common_options & options) {
        auto list_command = app.add_subcommand("list", "Get list of existing projects");
        list_command->fallthrough();
        list_command->callback([&]() {
            size_t namespace_id = 0;
            constexpr int PER_PAGE = 20;

            if (!options.namespace_name.empty() && options.namespace_name != "0") {
                namespace_id = get_namespace_id(connection, options.namespace_name, options.access_token);

                if (namespace_id == 0) {
                    BOOST_THROW_EXCEPTION(std::runtime_error("Can't find "+ options.namespace_name + " namespace"));
                }
            }

            if (namespace_id > 0) {
                for (int page = 1; ; ++page) {
                    auto r = connection.get("/api/v4/groups/" + std::to_string(namespace_id) + "/projects", {
                        {"Authorization", "Bearer " + options.access_token}
                    }, {
                        {"order_by", "name"},
                        {"sort", "asc"},
                        {"page", std::to_string(page)},
                        {"per_page", std::to_string(PER_PAGE)}
                    });

                    auto list = json::parse(r);

                    for (auto & project : list) {
                        std::cout << project["name"].get<std::string>() << ": " << project["ssh_url_to_repo"].get<std::string>() << std::endl;
                    }

                    if (list.size() < PER_PAGE) {
                        break;
                    }
                }
            } else {
                for (int page = 1; ; ++page) {
                    auto r = connection.get("/api/v4/projects", {
                        {"Authorization", "Bearer " + options.access_token}
                    }, {
                        {"owned", "true"},
                        {"order_by", "name"},
                        {"sort", "asc"},
                        {"page", std::to_string(page)},
                        {"per_page", std::to_string(PER_PAGE)}
                    });

                    auto list = json::parse(r);

                    for (auto & project : list) {
                        std::cout << project["name"].get<std::string>() << ": " << project["ssh_url_to_repo"].get<std::string>() << std::endl;
                    }

                    if (list.size() < PER_PAGE) {
                        break;
                    }
                }
            }
        });
    }
}
