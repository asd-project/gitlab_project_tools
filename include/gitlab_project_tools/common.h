//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <http/http_client.h>

#include <iostream>
#include <rang.hpp>

#include <CLI/CLI.hpp>

#include <fs/fs.h>
#include <container/function.h>

//---------------------------------------------------------------------------

namespace asd
{
    using json = nlohmann::json;

    struct command_context
    {
        template <class F>
        auto execute(F f) {
            auto cmd = f(*this);
            cmd.execute();

            return cmd;
        }

        std::string current_path;
    };

    struct command
    {
        command() {
            callback = [](command &){};
        }

        command(const std::string & s, const command_context & ctx) {
            callback = [=](command & c) {
                execute(c, s, ctx);
            };
        }

        command(const asd::function<void(command & c)> & f) {
            callback = f;
        }

        command operator && (command && second) {
            auto first = *this;

            return {[=](command & c) mutable {
                first.execute();

                c.result = first.result;
                c.std_out += first.std_out;
                c.std_err += first.std_err;

                if (c.result != 0) {
                    return;
                }

                second.execute();

                c.result = second.result;
                c.std_out += second.std_out;
                c.std_err += second.std_err;
            }};
        }

        command operator || (command && second) {
            auto first = *this;

            return {[=](command & c) mutable {
                first.execute();

                c.result = first.result;
                c.std_out += first.std_out;
                c.std_err += first.std_err;

                if (c.result == 0) {
                    return;
                }

                second.execute();

                c.result = second.result;
                c.std_out += second.std_out;
                c.std_err += second.std_err;
            }};
        }

        void execute() {
            callback(*this);
        }

        static void execute(command & cmd, const std::string & s, const command_context & ctx);

        asd::function<void(command & c)> callback;
        int result = 0;
        std::string std_out;
        std::string std_err;
    };

    inline json get_namespace_info(http::connection & connection, const std::string & namespace_name, const std::string & access_token) {
        auto r = connection.get("/api/v4/namespaces", {
            {"Authorization", "Bearer " + access_token}
        });

        auto list = json::parse(r);

        for (auto & ns : list) {
            if (ns["name"] == namespace_name) {
                return ns;
            }
        }

        return {};
    }

    inline size_t get_namespace_id(http::connection & connection, const std::string & namespace_name, const std::string & access_token) {
        auto info = get_namespace_info(connection, namespace_name, access_token);

        if (info.empty()) {
            return 0;
        }

        return info["id"].get<int>();
    }

    struct common_options
    {
        std::string access_token;
        std::string namespace_name;
    };
}
