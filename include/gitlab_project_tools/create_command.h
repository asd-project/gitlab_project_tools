//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gitlab_project_tools/common.h>

//---------------------------------------------------------------------------

namespace asd
{
    struct CreateOptions
    {
        std::string name;
        std::string description;
    };

    inline void add_create_command(CLI::App & app, http::connection & connection, const common_options & options) {
        auto create_options = std::make_shared<CreateOptions>();
        auto create_command = app.add_subcommand("create", "Create project");

        create_command->add_option("name", create_options->name, "Name of the project")->required();
        create_command->add_option("description", create_options->description, "Description");
        create_command->fallthrough();
        create_command->callback([&, create_options]() {
            std::string project_name = create_options->name;

            size_t namespace_id = 0;

            if (!options.namespace_name.empty() && options.namespace_name != "0") {
                namespace_id = get_namespace_id(connection, options.namespace_name, options.access_token);

                if (namespace_id == 0) {
                    BOOST_THROW_EXCEPTION(std::runtime_error("Can't find "+ options.namespace_name + " namespace"));
                }
            }

            json payload {
                {"name", project_name},
                {"description", create_options->description},
                {"visibility", "public"},
                {"merge_method", "ff"},
                {"default_branch", "development"}
            };

            if (namespace_id > 0) {
                payload["namespace_id"] = namespace_id;
            }

            auto r = connection.post("/api/v4/projects", {
                {"Authorization", "Bearer " + options.access_token}
            }, payload);

            auto project_info = json::parse(r);

            auto id = project_info["id"].get<int>();
            auto http_url = project_info["http_url_to_repo"].get<std::string>();

            std::cout << "Successfully created project \"" << create_options->name << "\"\n"
                      << "id: " << id << "\n"
                      << "http url: " << http_url << "\n";

            fs::path project_local_path(fs::current_path() / create_options->name);

            if (fs::exists(project_local_path)) {
                command_context ctx {project_local_path.string()};

                auto cmd = ctx.execute([&](const auto & ctx) {
                    command init;

                    if (!fs::exists(project_local_path / ".git")) {
                        init = command("git init", ctx) && command("git add .", ctx) && command("git commit -m \"Initial commit\"", ctx);
                    }

                    return init &&
                        (command("git remote set-url origin " + http_url, ctx) || command("git remote add origin " + http_url, ctx)) &&
                        command("git push -u origin HEAD", ctx) &&
                        command("git checkout -b development", ctx) &&
                        command("git push -u origin HEAD", ctx);
                });

                std::cout << cmd.std_out;
                std::cerr << cmd.std_err;

                json payload {
                    {"default_branch", "development"}
                };

                if (namespace_id > 0) {
                    payload["namespace_id"] = namespace_id;
                }

                auto r = connection.put("/api/v4/projects/" + std::to_string(id), {
                    {"Authorization", "Bearer " + options.access_token}
                }, payload);
            } else {
                std::cout << "Not found path \"" << create_options->name << "\"\n";
            }
        });
    }
}
