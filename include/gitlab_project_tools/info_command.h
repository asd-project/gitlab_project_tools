//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gitlab_project_tools/common.h>

//---------------------------------------------------------------------------

namespace asd
{
    struct InfoOptions
    {
        std::string name;
    };

    inline void add_info_command(CLI::App & app, http::connection & connection, const common_options & options) {
        auto info_options = std::make_shared<InfoOptions>();
        auto info_command = app.add_subcommand("info", "Get project info");

        info_command->add_option("name", info_options->name, "Name of the project")->required();
        info_command->fallthrough();
        info_command->callback([&, info_options]() {
            std::string project_name = info_options->name;

            if (!options.namespace_name.empty() && options.namespace_name != "0") {
                auto info = get_namespace_info(connection, options.namespace_name, options.access_token);

                if (info.empty()) {
                    BOOST_THROW_EXCEPTION(std::runtime_error("Can't find " + options.namespace_name + " namespace"));
                }

                project_name = info["full_path"].get<std::string>() + "/" + project_name;
            }

            auto r = connection.get("/api/v4/projects/" + http::url_encode(project_name), {
                {"Authorization", "Bearer " + options.access_token}
            });

            auto project = json::parse(r);
            auto name = project["name"].get<std::string>();

            std::cout << rang::fg::green << rang::style::bold << project["name_with_namespace"].get<std::string>() << rang::style::reset << "\n"
                      << rang::fg::green << "repo: " << rang::fg::reset << rang::style::underline << project["web_url"].get<std::string>() << rang::style::reset << "\n"
                      << rang::fg::green << "issues: " << rang::fg::reset << rang::style::underline << project["web_url"].get<std::string>() + "/issues" << rang::style::reset << " (open issues: " << project["open_issues_count"].get<int>() << ")"
                      << "\n"
                      << std::endl;
        });
    }
}
