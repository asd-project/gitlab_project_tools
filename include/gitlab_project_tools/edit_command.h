//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gitlab_project_tools/common.h>
#include <optional>

//---------------------------------------------------------------------------

namespace asd
{
    struct EditOptions
    {
        std::string name;
        std::optional<std::string> description;
        std::optional<std::string> default_branch;
    };

    inline void add_edit_command(CLI::App & app, http::connection & connection, const common_options & options) {
        auto edit_options = std::make_shared<EditOptions>();
        auto edit_command = app.add_subcommand("edit", "Edit project");

        edit_command->add_option("name", edit_options->name, "Name of the project")->required();
        edit_command->add_option("--description", edit_options->description, "Description");
        edit_command->add_option("--default_branch", edit_options->default_branch, "Default branch");
        edit_command->fallthrough();
        edit_command->callback([&, edit_options]() {
            std::string project_name = edit_options->name;

            if (!options.namespace_name.empty() && options.namespace_name != "0") {
                auto info = get_namespace_info(connection, options.namespace_name, options.access_token);

                if (info.empty()) {
                    BOOST_THROW_EXCEPTION(std::runtime_error("Can't find " + options.namespace_name + " namespace"));
                }

                project_name = info["full_path"].get<std::string>() + "/" + project_name;
            }

            json payload {};

            if (edit_options->description) {
                payload["description"] = *edit_options->description;
            }

            if (edit_options->default_branch) {
                payload["default_branch"] = *edit_options->default_branch;
            }

            auto r = connection.put("/api/v4/projects/" + http::url_encode(project_name), {
                {"Authorization", "Bearer " + options.access_token}
            }, payload);

            std::cout << "Successfully edited project \"" << edit_options->name << "\"\n";
        });
    }
}
