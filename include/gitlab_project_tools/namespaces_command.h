//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <gitlab_project_tools/common.h>

//---------------------------------------------------------------------------

namespace asd
{
    inline void add_namespaces_command(CLI::App & app, http::connection & connection, const common_options & options) {
        auto namespaces_command = app.add_subcommand("namespaces", "Get current user namespaces");
        namespaces_command->fallthrough();
        namespaces_command->callback([&]() {
            auto r = connection.get("/api/v4/namespaces",  {
                {"Authorization", "Bearer " + options.access_token}
            });

            std::cout << json::parse(r).dump(4) << std::endl;
            return 0;
        });
    }
}
