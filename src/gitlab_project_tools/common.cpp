//---------------------------------------------------------------------------

#define sigtimedwait sigtimedwait

#include <algorithm>

#include <boost/process.hpp>
#include <boost/process/start_dir.hpp>
#include <gitlab_project_tools/common.h>

//---------------------------------------------------------------------------

namespace asd
{
    void command::execute(command & cmd, const std::string & s, const command_context & ctx) {
        namespace p = boost::process;

        p::ipstream out_stream;
        p::ipstream err_stream;

        if (ctx.current_path.empty()) {
            cmd.result = p::system(s, p::std_out > out_stream, p::std_err > err_stream);
        } else {
            cmd.result = p::system(s, p::std_out > out_stream, p::std_err > err_stream, p::start_dir = ctx.current_path);
        }

        std::string out_line;
        std::string err_line;

        while ((out_stream && getline(out_stream, out_line)) || (err_stream && getline(err_stream, err_line))) {
            if (!out_line.empty()) {
                cmd.std_out += out_line + "\n";
                out_line.clear();
            }

            if (!err_line.empty()) {
                cmd.std_err += err_line + "\n";
                err_line.clear();
            }
        }
    }
}

//---------------------------------------------------------------------------
