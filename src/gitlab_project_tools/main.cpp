// ---------------------------------------------------------------------------

#include <gitlab_project_tools/create_command.h>
#include <gitlab_project_tools/edit_command.h>
#include <gitlab_project_tools/info_command.h>
#include <gitlab_project_tools/list_command.h>
#include <gitlab_project_tools/namespaces_command.h>

#include <flow/run_main.h>
#include <launch/main.h>

// ---------------------------------------------------------------------------

namespace asd
{
    static auto process_args(http::connection & connection, const std::vector<std::string_view> & args) {
        CLI::App app {"gitlab projects management utility"};
        app.require_subcommand(1);
        app.failure_message(CLI::FailureMessage::help);

        common_options options;

        auto config_path = application::root_path() / "config.toml";

        app.add_option("--token", options.access_token, "Account access token")->required();
        app.add_option("--namespace", options.namespace_name, "Namespace name");
        app.set_config("--config", config_path.string(), "Config file (.toml) with default options values", true);

        add_create_command(app, connection, options);
        add_edit_command(app, connection, options);
        add_info_command(app, connection, options);
        add_list_command(app, connection, options);
        add_namespaces_command(app, connection, options);

        try {
            std::vector<std::string> app_args{args.rbegin(), std::prev(args.rend())};

            app.parse(std::move(app_args));
        } catch (const CLI::ParseError & e) {
            return app.exit(e);
        }

        return 0;
    }

    static auto e = [](int argc, char ** argv) {
        std::vector<std::string> args;

        boost::asio::io_context io_context;

        std::string host = "https://gitlab.com";
        int result = 0;

        try {
            http::connection_options options;
            options.retry_attempts = 5;

            http::connect(host, io_context, options, [&](http::connection & connection) {
                try {
                    result = process_args(connection, application::arguments());
                } catch (const std::runtime_error & e) {
                    std::cerr << "Error: " << e.what() << std::endl;
                }
            });
        } catch (const std::exception & e) {
            std::cerr << "Error: " << e.what() << std::endl;
            return 1;
        }

        return result;
    };

    static entrance open(e);
}

// ---------------------------------------------------------------------------
